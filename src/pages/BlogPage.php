<?php

namespace {
    use SilverStripe\Assets\Image;
    use TWA\Blog\Model\Post;
    class BlogPage extends Page {     
        private static $belongs_many_many = [
            'CuratedPosts' => Post::class
        ];

        private static $has_one = [
            'TagPageImage' => Image::class,
            'CategoryPageImage' => Image::class,
        ];        

        private static $owns = [
            'TagPageImage',
            'CategoryPageImage'
        ];
    
    }

}
