<?php

use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\RSS\RSSFeed;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Convert;
use SilverStripe\Dev\Debug;
use SilverStripe\ORM\FieldType\DBDatetime;
use SilverStripe\ORM\PaginatedList;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Versioned\Versioned;
use SilverStripe\View\ArrayData;
use SilverStripe\View\Parsers\ShortcodeParser;
use SilverStripe\View\Parsers\URLSegmentFilter;
use TWA\Blog\Model\Category;
use TWA\Blog\Model\Tag;
use TWA\Blog\Model\Post;

class BlogPageController extends PageController
{
    const LIST_MAX_COUNT = 10;

    private static $allowed_actions = [
        'index',
        'view',
        'list',
        'archive',
        'tag',
        'category',
        'rss',
        'feed',
    ];

    private static $url_handlers = [
        'rss'                               => 'rss',
        'tag/$Tag!/$Rss'                    => 'tag',
        'category/$Category!/$Rss'          => 'category',
        'archive/$Year!/$Month/$Day'        => 'archive',
        'list/$Page!/$Limit!/$Key/$Value'   => 'list',
        '$Action!'                          => 'view',
    ];

    private static $casting = [
        'MetaTitle'         => 'Text',
        'MetaDescription'   => 'Text'
    ];

    protected $current_post;
    protected $current_category;
    protected $current_tag;

    protected $active_year;
    protected $active_month;
    protected $active_day;
    protected $rss_limit = 10;

    protected $blog_posts;

    protected function init() {
        parent::init();
        $this->rss_limit = Config::inst()->get(__CLASS__, 'rss_post_limit') ?: 10;
    }    

    public function index(HTTPRequest $request)
    {
        $data = [
            'Posts'         => $this->getPublishedPosts(),
            'Categories'    => $this->getCategories()
        ];
        return $this->render($data);
    }

    # +------------------------------------------------------------------------+
    # ALLOWED ACTIONS
    # +------------------------------------------------------------------------+
    public function view() {
        if( $post = $this->getCurrentPost() ) {
            $this->Title    = ucwords($post->Title);
            $this->Content  = ShortcodeParser::get_active()->parse($post->Content);
            $this->Description     = $post->Summary;
            $this->Post     = $post;
            $this->setContentSource($post);

            return $this->render();
        }
           
        $this->httpError(404, 'Not Found');

        return $this->render();    
    }

    public function list() {

        $allowed_keys = ['category','tag'];
        $is_archive = false;

        $page   = (int) Convert::raw2sql(filter_var($this->request->param('Page'), FILTER_SANITIZE_NUMBER_INT) ?: 0); // IMPORTANT: This MUST BE zero based
        $limit  = (int) Convert::raw2sql(filter_var($this->request->param('Limit'), FILTER_SANITIZE_NUMBER_INT) ?: self::LIST_MAX_COUNT);
        $key    = Convert::raw2sql(filter_var($this->request->param('Key'), FILTER_SANITIZE_STRING) ?: null); // Accept category or tag + a value
        $value  = Convert::raw2sql(filter_var($this->request->param('Value'), FILTER_SANITIZE_STRING) ?: null);

        if( $this->hasExtension('TWA\\Extension\\ControllerExtension') ) {
            $is_archive = !!($key && $value) && in_array($key, $allowed_keys);
            if( $is_archive ) {
                switch( $key ) {
                    case 'tag':
                        $posts_list = $this->getTagFromSegment('Value');
                        break;
                    case 'category':
                    default:
                        $posts_list = $this->getCategoryFromSegment('Value');
                        break;
                }
                $posts_list = $posts_list->getPosts()->filter(['PublishedDate:LessThan' => DBDatetime::now()->modify("+ 1 day")])->sort('PublishedDate','DESC');                
            }else{
                $posts_list = Post::get()->filter(['PublishedDate:LessThan' => DBDatetime::now()->modify("+ 1 day")])->sort('PublishedDate','DESC');
            }
            
            // PaginatedList
            $paginated_list = PaginatedList::create($posts_list, $this->request);
            // $paginated_list->setLimitItems(false); // Uncomment this line to disable automatic limiting - this is handled in the ORM call above
            $paginated_list->setPageStart(0);
            $paginated_list->setPageLength($limit);
            $paginated_list->setCurrentPage($page);
            
            // Convert posts to tiles
            if( $paginated_list ) {
                $posts = array_map(function($v) {
                    $post = $v->asTiledPost()->getValue();
                    return $post;
                }, $paginated_list->getList()->limit($limit,($limit * $page))->toArray());
            }

            // Load more component
            $blog_page  = BlogPage::get()->First();
            $blog_url   = Controller::join_links( Director::absoluteBaseUrl(), !!$blog_page ? $blog_page->Link() : null );
            $link       = sprintf("%s/list/%s/%s",rtrim($blog_url,"/"),( !!count($posts) ? $paginated_list->CurrentPage() + 1 : 0 ),$limit);
            if( $is_archive ) {
                $link       = sprintf("%s/%s/%s",$link,$key,URLSegmentFilter::create()->filter($value));
            }
            $loadmore = $this->renderWith("Includes\\BlogPage_LoadMore", [
                'Page'      => $paginated_list->CurrentPage(),
                'Limit'     => $limit,
                'Link'      => $link
            ]);
                
            $response = [
                'posts'     => $posts,
                'page'      => ( !!count($posts) ? $paginated_list->CurrentPage() + 1 : 0 ),
                'pages'     => $paginated_list->TotalPages(),
                'more'      => $paginated_list->MoreThanOnePage() ? $loadmore->getValue() : null
            ];

            return call_user_func_array([$this,'getAPIResponse'], [$response, 200]);
        }

        $this->httpError(400, 'Bad Request');

        return $this->render();        
    }

    public function archive() {
        $this->Title        = ucwords(__FUNCTION__);
        
        $m = $this->active_year = $this->getArchiveMonth();
        $d = $this->active_month = $this->getArchiveDay();
        $y = $this->active_day = $this->getArchiveYear();

        if ($this->request->param('Month') && !$m) {
            $this->httpError(404, 'Not Found');
        }

        if ($m && $this->request->param('Day') && !$d) {
            $this->httpError(404, 'Not Found');
        }

        if (!!$y) {

            $blog_posts = Versioned::get_by_stage(Post::class,Versioned::LIVE);

            // Filter by Year
            $query_year = sprintf("Year(PublishedDate)=%s", $y);
            $blog_posts = $blog_posts->where( $query_year );
            
            // Filter by Month
            if( $m ) {
                $query_month    = sprintf("Month(PublishedDate)=%s", $m);
                $blog_posts     = $blog_posts->where( $query_month );
            }

            // Filter by Day
            if( $d ) {
                $query_day  = sprintf("Day(PublishedDate)=%s", $d);
                $blog_posts = $blog_posts->where( $query_day );
            }

            $this->Posts = $blog_posts;
            $this->setContentSource($blog_posts);

            return $this->render();
        }        
        
        $this->httpError(404, 'Not Found');

        return $this->render();
    }
    
    public function tag() {
        $this->current_tag = $this->getTagFromSegment();
        if( $blog_posts = $this->current_tag ) {
            $blog_posts = $blog_posts->getPosts();

            if( !$this->isRSS() ) {
                $this->Title = sprintf("%s Archives", $this->current_tag->Title);
                $this->Tag = $this->current_tag;
                $this->Posts = $blog_posts;
                $this->setContentSource($this->current_tag);
            }
    
            return $this->isRSS() ? $this->feed($blog_posts, $this->rss_limit) : $this->render();
        }
        $this->httpError(404, 'Not Found');
    }

    public function category() {
        $this->current_category = $this->getCategoryFromSegment();
        if( $blog_posts = $this->current_category ) {
            $blog_posts = $blog_posts->getPosts();            

            if( !$this->isRSS() ) {
                $this->Title = sprintf("%s Archives", $this->current_category->Title);
                $this->Tag = $this->current_category;
                $this->Posts = $blog_posts;
                $this->setContentSource($this->current_category);
            }

            return $this->isRSS() ? $this->feed($blog_posts, $this->rss_limit) : $this->render();
        }

        $this->httpError(404, 'Not Found');
    }

    public function rss() {
        $blog_posts = Versioned::get_by_stage(Post::class,Versioned::LIVE);        
        return $this->feed( $blog_posts, $this->rss_limit );
    }

    # +------------------------------------------------------------------------+
    # CONTENT/LINKS
    # +------------------------------------------------------------------------+
    public function AbsoluteLink() 
    {
        return Controller::join_links( Director::absoluteBaseUrl(),$this->URLSegment );
    }    

    # +------------------------------------------------------------------------+
    # HELPER METHODS
    # +------------------------------------------------------------------------+
    protected function isRSS()
    {
        $rss = filter_var($this->request->param('Rss'), FILTER_SANITIZE_STRING);
        return (is_string($rss) && strcasecmp($rss, 'rss') == 0);
    }

    /**
     * Generate a 
     */
    protected function feed( $blog_posts, $limit = 10 ) 
    {
        $site_config    = SiteConfig::current_site_config();
        $posts          = $blog_posts->limit($limit);

        $config     = SiteConfig::current_site_config(); 
        $blog_page  = BlogPage::get()->First();
        $blog_url   = Controller::join_links( Director::absoluteBaseUrl(), !!$blog_page ? $blog_page->Link() : null );
        $is_archive = !!($this->getAction() == 'category' || $this->getAction() == 'tag');
        $link       = $is_archive ? sprintf("%s/%s/%s/rss/",rtrim($blog_url,"/"),$this->getAction(),$this->getRequest()->param(ucwords($this->getAction()))) : sprintf("%s/rss/",$this->AbsoluteLink());

        $rss_feed = new RSSFeed(
            $posts,
            $link,
            $site_config->Title,
            sprintf("Latest posts from %s", $site_config->Title),
            'Title',
            'Summary',
            'Author.FirstName',
            'PublishedDate'
        );

        $this->getResponse()->addHeader('Content-type', 'application/rss+xml');

        $output = str_replace("<?xml version=\"1.0\"?>","",$rss_feed->outputToBrowser());
        return trim($output);
    }

    public function getCurrentPost( $url_segment = null ) {
        $segment = null != $url_segment ? $url_segment : $this->request->param('Action');
        $this->current_post = Post::get()->filter(['URLSegment' => $segment ])->first();
        return $this->current_post;
    }

    /**
     * Get a list of published posts
     */
    protected function getPublishedPosts( $limit = -1, $offset = 0 ) {
        $posts = Post::get();
        $posts = $posts->filter(['PublishedDate:LessThan' => DBDatetime::now()->modify("+ 1 day")]);
        $posts = $posts->sort('PublishedDate','DESC');
        if( $limit > 0 ) {
            $posts = $posts->limit($limit, $offset);
        }
        return $posts;
    }

    protected function getCategories() {
        $categories = Category::get()->sort('Title', 'ASC');
        return $categories;
    }

    /**
     * Get a Tag dataobject from a url segment
     *
     * @return Tag
     */
    protected function getTagFromSegment( $param_key = 'Tag' ) {
        $tag = $this->request->param(ucwords($param_key));
        if( !$tag ) return null;
        $filter = URLSegmentFilter::create();
        if (!$filter->getAllowMultibyte()) {
            $tag = rawurlencode($tag);
        }
        return Tag::get()->filter('URLSegment', $tag)->first();
    }
    
    /**
     * Get a Category dataobject from a url segment
     *
     * @return Category
     */
    protected function getCategoryFromSegment( $param_key = 'Category' ) {
        $cat = $this->request->param(ucwords($param_key));
        if( !$cat ) return null;
        $filter = URLSegmentFilter::create();
        if (!$filter->getAllowMultibyte()) {
            $cat = rawurlencode($cat);
        }
        return Category::get()->filter('URLSegment', $cat)->first();
    }

    /**
     * Get a valid year from the Year segment
     *
     * @return int
     */
    protected function getArchiveYear() {
        $year = filter_var($this->request->param('Year'), FILTER_SANITIZE_NUMBER_INT);
        if ($year) {
            if (preg_match('/^[0-9]{4}$/', $year)) {
                return (int) $year;
            }
        } elseif ($this->request->param('Action') == 'archive') {
            return DBDatetime::now()->Year();
        }
        return null;
    }    

    /**
     * Get a valid month from the Month segment
     *
     * @return int
     */
    protected function getArchiveMonth() {
        $month = filter_var($this->request->param('Month'), FILTER_SANITIZE_NUMBER_INT);
        if (preg_match('/^[0-9]{1,2}$/', $month)) {
            if ($month > 0 && $month <= 12) {
                if (checkdate($month, 01, $this->getArchiveYear())) {
                    return (int) $month;
                }
            }
        }
        return null;
    }

    /**
     * Get a valid day from the Day segment
     *
     * @return int
     */
    protected function getArchiveDay() {
        $day = filter_var($this->request->param('Day'), FILTER_SANITIZE_NUMBER_INT);
        if (preg_match('/^[0-9]{1,2}$/', $day)) {
            if (checkdate($this->getArchiveMonth(), $day, $this->getArchiveYear())) {
                return (int) $day;
            }
        }
        return null;
    }    
}