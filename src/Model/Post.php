<?php

namespace TWA\Blog\Model;

use BlogPage;
use \PageController;
use SilverStripe\Forms\SegmentField;
use SilverStripe\Forms\SegmentFieldModifier\IDSegmentFieldModifier;
use SilverStripe\Forms\SegmentFieldModifier\SlugSegmentFieldModifier;
use SilverStripe\CMS\Forms\SiteTreeURLSegmentField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\ToggleCompositeField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Member;
use SilverStripe\Security\Permission;
use SilverStripe\Security\Security;
use SilverStripe\SnapshotAdmin\SnapshotHistoryExtension;
use SilverStripe\TagField\TagField;
use SilverStripe\Versioned\Versioned;
use TWA\Utils\Text;

class Post extends DataObject {
    const EXCERPT_WORD_COUNT        = 55;
    const METADESCRIPTION_LENGTH    = 155;

    private static $table_name = 'Post';

    private static $versioned_gridfield_extensions = true;

    private static $extensions = [
        Versioned::class,
        SnapshotHistoryExtension::class
    ];

    private static $db = [
		'Title'             => 'Varchar(255)',
		'Content'           => 'HTMLText',
        'PublishedDate'     => 'DBDatetime',
        'Summary'           => "Text",
        'URLSegment'        => 'Varchar(200)',
        // "MetaTitle"         => "Varchar(255)",
        // "MetaDescription"   => "Text",
        'SortOrder'         => 'Int', // Field needed for sorting Curated Posts
    ];

    private static $indexes = [
        'PublishedDate' => true,
        'Title_Index' => [
            'type' => 'unique', 
            'columns' => ['Title'],
        ],
        'URLSegment_Index' => [
            'type' => 'unique', 
            'columns' => ['URLSegment'],
        ],
    ];

    private static $casting = [
        'Content' => 'HTMLText'
    ];

    private static $has_one = [
        'Image'     => Image::class,
        'Author'    => Member::class        
    ];

    private static $has_many = [
    ];

    private static $many_many = [
        'Categories'    => Category::class,
        'Tags'          => Tag::class,
        'BlogPages'     => BlogPage::class
    ];  
    
    private static $owns = [
        'Image'
    ];

    private static $default_sort = '"PublishedDate" IS NULL DESC, "PublishedDate" DESC';    

	private static $summary_fields = [
        'Title'         => 'Title',
        'PublishedDate'     => 'PublishedDate',
        'Created'       => 'Created',
        'LastEdited'    => 'LastEdited'
    ]; 

    # +------------------------------------------------------------------------+
    # CONTENT/LINKS
    # +------------------------------------------------------------------------+
    public function AbsoluteLink() 
    {
        $blog_page = BlogPage::get()->First();
        return Controller::join_links( Director::absoluteBaseUrl(), !!$blog_page ? $blog_page->Link() : null, $this->URLSegment );
    }

    public function Link() {
        $blog_page = BlogPage::get()->First();
        return Controller::join_links( !!$blog_page ? $blog_page->Link() : null, $this->URLSegment );
    } 

    # +------------------------------------------------------------------------+
    # CMS FIELDS
    # +------------------------------------------------------------------------+
    public function updateCMSFields(FieldList $fields) {
        parent::updateCMSFields($fields);
        return $fields;        
    }
    

    public function getCMSFields() {
        
        $fields = parent::getCMSFields();

        # +------------------------------------------------------------------------+
        # URL SEGMENT
        # +------------------------------------------------------------------------+      
        if (!!$this->ID) {
            $url_segment_field  = new SiteTreeURLSegmentField('URLSegment', 'Slug');
            $host_page          = \PageController::PageLinkByClass('BlogPage');
            $prefix             = Controller::join_links( Director::absoluteBaseUrl(), $host_page);
            
            $url_segment_field->setURLPrefix($prefix);
            $helpText = _t('SiteTreeURLSegmentField.HelpChars', ' Special characters are automatically converted or removed.');
            $url_segment_field->setHelpText($helpText);
            $fields->insertAfter('Title', $url_segment_field);
        }
        
        $featured_image = $fields->dataFieldByName('Image');
        $featured_image->getValidator()->setAllowedExtensions(array('jpg', 'jpeg', 'png'));    
        $featured_image->setFolderName('post');            
        
        $url_segment_field = $fields->dataFieldByName('URLSegment');
        if( $this->ID ) {
            $url_segment_field->setTitle('Slug');
            $fields->insertAfter('Title', $url_segment_field);
        }else{
            $fields->removeByName('URLSegment');
        }
        
        $content_field = $fields->dataFieldByName('Content');
        $content_field->addExtraClass('stacked');

        // New Settings Tab
        $fields->addFieldsToTab('Root.Settings', [
            $fields->dataFieldByName('AuthorID'), 
            $fields->dataFieldByName('PublishedDate'), 
            $featured_image = UploadField::create('Image','Featured Image'),
        ]);

        // Change Categories to checkboxes
        if( $this->ID ) {
            // $category_list = Category::get()->map('ID','Title');
            // $fields->replaceField('Categories', new CheckboxSetField('Categories', 'Categories', $category_list));
        }

        // Create Settings tab
        $fields->fieldByName('Root.Settings')->setTitle('Settings');

        # +------------------------------------------------------------------------+
        # TAXONOMY
        # +------------------------------------------------------------------------+
        // Remove empty tabs
        $fields->removeByName( 'SortOrder' );
        $fields->removeByName( 'Tags' );
        $fields->removeByName( 'Categories' );

        $tag_field = TagField::create(
            'Tags',
            'Tags',
            Tag::get(),
            $this->Tags()
        );
        $tag_field->setShouldLazyLoad(true);
        $tag_field->setCanCreate(true);

        $category_field = TagField::create(
            'Categories',
            'Categories',
            Category::get(),
            $this->Categories()
        );
        $category_field->setShouldLazyLoad(true);
        $category_field->setCanCreate(true);

        $taxonomy_holder = ToggleCompositeField::create(
            'Taxonomy',
            'Taxonomy',[
                $tag_field,
                $category_field
            ]
        );
        $taxonomy_holder->setHeadingLevel(4);
        $taxonomy_holder->addExtraClass('custom-taxonomy');

        $fields->addFieldToTab('Root.Main',$taxonomy_holder, 'Taxonomy');        

        # +------------------------------------------------------------------------+
        # METADATA
        # +------------------------------------------------------------------------+        
        $summary_field = $fields->dataFieldByName('Summary');
        $summary_field->setRightTitle('Provide a custom excerpt up to 55 words. This summary will be used in site search results.');

        // $metatitle_field = $fields->dataFieldByName('MetaTitle');
        // $metatitle_field->setRightTitle('Override the post title with a custom one.');

        // $metadescription_field = $fields->dataFieldByName('MetaDescription');
        // $metadescription_field->setRightTitle('Provide a custom metadata description up to 155-160 characters. If provided, this custom description will override the automatically generated one.');

        // $metadata_holder = ToggleCompositeField::create(
        //     'Metadata',
        //     'Metadata',[
        //         $summary_field,
        //         $metatitle_field,
        //         $metadescription_field
        //     ]
        // );
        // $metadata_holder->setHeadingLevel(4);
        // $metadata_holder->addExtraClass('custom-metadata');

        // Remove fields
        // $fields->removeFieldFromTab('Root.Main','Summary');
        // $fields->removeFieldFromTab('Root.Main','MetaTitle');
        // $fields->removeFieldFromTab('Root.Main','MetaDescription');

        // if($this->Summary) {
        //     $metadata_holder->setStartClosed(false);
        // }

        // $fields->addFieldsToTab('Root.Main', $metadata_holder);
		
        return $fields;
    }

    # +------------------------------------------------------------------------+
    # EVENTS
    # +------------------------------------------------------------------------+
    public function onBeforeWrite() {
        parent::onBeforeWrite();

        // Update the URL segment
		if(empty($this->URLSegment)) {
            $this->URLSegment = \PageController::URLSegmentFilter($this->Title);
            $this->URLSegment = trim($this->URLSegment);
        }

        // Author if not selected
        if( !$this->AuthorID && Security::getCurrentUser() ) {
            $this->AuthorID = Security::getCurrentUser()->ID;
        }
        
        // Generate a summary if none is provided.
        if( empty($this->Summary) ) {
            $this->Summary = $this->Excerpt();
        }
        
    }

    # +------------------------------------------------------------------------+
    # METHODS
    # +------------------------------------------------------------------------+
    /**
     * Get the title for this post. If a MetaTitle is provided, then it will be
     * used in place of the original post title. This does not affect the slug.
     *
     * @return void
     */
    public function getPostMetaTitle() {
        return $this->MetaTitle ?: $this->getField('Title');
    }

    /**
     * Get a meta description of this post as either plain-text or
     * an HTML <meta /> description tag.
     *
     * @param boolean $as_tag
     * @return void
     */
    public function getPostMetaDescription( $as_tag = false ) {
        $description = substr($this->MetaDescr, 0, self::METADESCRIPTION_LENGTH);
        if( $as_tag ) {
            $description = sprintf('<meta name="description" content="%s">', $description);
        }
        return $description;
    }

    /**
     * Get a plain-text exerpt for this post. If a custom summary is not provided,
     * then a truncated excerpt of the main content is used.
     *
     * @return string
     */
    public function Excerpt() {
        return Text::truncateWords($this->Summary ?? $this->Content, 0, self::EXCERPT_WORD_COUNT, '');
    }

    # +------------------------------------------------------------------------+
    # EVENTS
    # +------------------------------------------------------------------------+
    public function onAfterWrite()
    {
        if ($this->Image()->exists() && !$this->Image()->isPublished()) {
            $this->Image()->doPublish();
        }
        parent::onAfterWrite();
    }

    # +------------------------------------------------------------------------+
    # VALIDATION
    # +------------------------------------------------------------------------+
    public function validate() {
        $result = parent::validate();

        if (!$this->ID) {
            if( $record = Post::get()->filter( ['Title' => $this->Title] )->first() ) {
                if( $record->Title == $this->Title ) {
                    $result->addFieldError('Title',sprintf('A post with the name, "%s", already exists.', $this->Title));
                }
            }
        }

        return $result;
    }    

    # +------------------------------------------------------------------------+
    # PERMISSIONS
    # +------------------------------------------------------------------------+
    public function canView($member = null) 
    {
        return Permission::check('CMS_ACCESS_BlogAdmin', 'any', $member);
    }

    public function canEdit($member = null) 
    {
        return Permission::check('CMS_ACCESS_BlogAdmin', 'any', $member);
    }

    public function canDelete($member = null) 
    {
        return Permission::check('CMS_ACCESS_BlogAdmin', 'any', $member);
    }

    public function canCreate($member = null, $context = []) 
    {
        return Permission::check('CMS_ACCESS_BlogAdmin', 'any', $member);
    } 
}
