<?php

namespace TWA\Blog\Model;

use BlogPage;
use \PageController;
use SilverStripe\CMS\Forms\SiteTreeURLSegmentField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Permission;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use TWA\Extension\DataObjectManyManyCleanup;

class Category extends DataObject {
    private static $table_name = 'Category';
    
    private static $db = [
        'Title'         => 'Varchar(255)',
        'SortOrder'     => 'Int',
        'URLSegment'    => 'Varchar(200)'        
    ];

    private static $indexes = [
        'SortOrder' => true,
        'Title_Index' => [
            'type' => 'unique', 
            'columns' => ['Title'],
        ],
        'URLSegment_Index' => [
            'type' => 'unique', 
            'columns' => ['URLSegment'],
        ],
    ];        
    
    private static $belongs_many_many = [
        'Posts' => Post::class
    ];
    
    private static $extensions = [
        DataObjectManyManyCleanup::class
    ];
    
    private static $default_sort = 'SortOrder ASC';

        
    private static $searchable_fields = [
        'Title'
    ];
    
    private static $summary_fields = [
        'Title' => 'Title',
        'Posts.Count' => 'Posts'
    ];
    
    private static $field_labels = [
        'Title' => 'Title'        
    ];
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        
        $fields->removeByName('SortOrder');

        # +------------------------------------------------------------------------+
        # URL SEGMENT
        # +------------------------------------------------------------------------+      
        if (!!$this->ID) {
            $url_segment_field  = new SiteTreeURLSegmentField('URLSegment', 'Slug');
            $host_page          = \PageController::PageLinkByClass('BlogPage');
            $prefix             = Controller::join_links( Director::absoluteBaseUrl(), $host_page, 'category');
            $prefix             = sprintf("%s/", rtrim($prefix,'/'));
            
            $url_segment_field->setURLPrefix($prefix);
            $helpText = _t('SiteTreeURLSegmentField.HelpChars', ' Special characters are automatically converted or removed.');
            $url_segment_field->setHelpText($helpText);
            $fields->insertAfter('Title', $url_segment_field);
        }        
        
        return $fields;
    }

    protected function onBeforeWrite() {
		parent::onBeforeWrite();
		
        if (empty($this->URLSegment)) {
            $this->URLSegment = PageController::URLSegmentFilter($this->Title);
        }
	}
    
    public function getPosts() {
        return $this->Posts()->sort('PublishedDate DESC');
    }
    
    public function getName() {
        return $this->Title;
    }
    
    # +------------------------------------------------------------------------+
    # CONTENT/LINKS
    # +------------------------------------------------------------------------+
    public function AbsoluteLink() 
    {
        $blog_page = BlogPage::get()->First();
        return Controller::join_links( Director::absoluteBaseUrl(), !!$blog_page ? $blog_page->Link() : null, 'category', $this->URLSegment );
    }

    public function Link() {
        $blog_page = BlogPage::get()->First();
        return Controller::join_links( !!$blog_page ? $blog_page->Link() : null, 'category', $this->URLSegment );
    }    

    # +------------------------------------------------------------------------+
    # VALIDATION
    # +------------------------------------------------------------------------+
    public function validate() {
        $result = parent::validate();

        if (!$this->ID) {
            if( $record = Post::get()->filter( ['Title' => $this->Title] )->first() ) {
                if( $record->Title == $this->Title ) {
                    $result->addFieldError('Title',sprintf('A category with the name, "%s", already exists.', $this->Title));
                }
            }
        }

        return $result;
    }    

    # +------------------------------------------------------------------------+
    # PERMISSIONS
    # +------------------------------------------------------------------------+
    public function canView($member = null) 
    {
        return Permission::check('CMS_ACCESS_BlogAdmin', 'any', $member);
    }

    public function canEdit($member = null) 
    {
        return Permission::check('CMS_ACCESS_BlogAdmin', 'any', $member);
    }

    public function canDelete($member = null) 
    {
        return Permission::check('CMS_ACCESS_BlogAdmin', 'any', $member);
    }

    public function canCreate($member = null, $context = []) 
    {
        return Permission::check('CMS_ACCESS_BlogAdmin', 'any', $member);
    }     
}
