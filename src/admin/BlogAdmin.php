<?php

use SilverStripe\Admin\ModelAdmin;
use TWA\Blog\Model\Category;
use TWA\Blog\Model\Post;
use TWA\Blog\Model\Tag;

class BlogAdmin extends ModelAdmin {
	
	private static $url_segment = "blog";
    
    private static $managed_models = [
        Post::class => ['title' => 'Posts', 'segment' => 'posts' ],
        Category::class => ['title' => 'Categories', 'segment' => 'categories'], 
        Tag::class => ['title' => 'Tags', 'segment' =>'tags']
     ] ;
    
    private static $menu_title = "Blog";
    
    public $showImportForm = false;

    private static $menu_icon_class = 'font-icon-book-open';

}